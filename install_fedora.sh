#!/bin/bash

install_folder=$(dirname $(readlink -f $0)) 

#sudo pacman -S python ipython python-virtualenv wget
#sudo pacman -S tk for plotting matplotlib

# Make sure systemclock is running



# COPY XORG CONFIG FILE TO HOMEFOLDER
cp ${install_folder}/xorg/.xinitrc $HOME

####### Vim #######
function install_vim {
    # Youcompleteme needs cmake
    echo COPYING VIM CONFIG TO $HOME/.vimrc
    cp $install_folder/vim/.vimrc $HOME/
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    #pip install flake8
    echo COPYING OLD-HOPE COLOR TO VIM CONFIG
    cp -rf $install_folder/vim/autoload $HOME/.vim/
    cp -rf $install_folder/vim/colors $HOME/.vim/
    cp -rf $install_folder/vim/UltiSnips $HOME/.vim/

    vim +PluginInstall +qall
    youcompleteme_install=$HOME/.vim/bundle/youcompleteme/install.py
    # IF NOT C++ needed install with  python $youcompleteme_install
    python $youcompleteme_install --clang-completer
    cp $install_folder/vim/global_extra_conf.py $HOME/
}

# WINDOW MANAGER i3-wm
function install_i3wm {
    echo COPY i3 CONFIG TO HOME DIRECTORY
    cp $install_folder/i3wm/config $HOME/.config/i3/config
}


####### TERMINAL urxvt ######
function install_urxvt {
        echo COPYING URXVT CONFIG FILE TO $HOME/.Xdefaults
        cp -r $install_folder/urxvt/.Xdefaults $HOME/
        mkdir -p $HOME/.urxvt/etx && cp $install_folder/urxvt/scripts/* $HOME/.urxvt/etx
    }

    install_vim
    install_i3wm
    install_urxvt
    #install_privacy_tools
