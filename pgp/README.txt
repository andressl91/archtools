#GENERATE NEW KEY
gpg --full-gen-key

# LIST KEYS RELATED TO EMAIL
gpg --list-secret-keys --keyid-format LONG <your_email>

# TO GET PUBLIC KEY FROM A PAIR
Copy the GPG key ID that starts with sec. In the following example, that’s 30F2B65B9246B6CA:
sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
      D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
uid                   [ultimate] Mr. Robot <your_email>
ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]

gpg --armor --export 30F2B65B9246B6CA

## RELATED TO GIT
# REMEMEBER! pgp email AND git config user.email must match
# Set signkey
git config --local user.signingkey 30F2B65B9246B6CA

#Sign commit
git commit -S -m "My commit msg"

#Sign every commit automatic
git config --local commit.gpgsign true

