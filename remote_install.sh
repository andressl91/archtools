#!/bin/bash

install_folder=$(dirname $(readlink -f $0)) 


####### Vim #######
function install_vim {
    # Youcompleteme needs cmake
    echo COPYING VIM CONFIG TO $HOME/.vimrc
    cp $install_folder/vim/.vimrc $HOME/
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    pip install flake8
    echo COPYING OLD-HOPE COLOR TO VIM CONFIG
    cp -rf $install_folder/vim/autoload $HOME/.vim/
    cp -rf $install_folder/vim/colors $HOME/.vim/
    cp -rf $install_folder/vim/UltiSnips $HOME/.vim/

    vim +PluginInstall +qall
    youcompleteme_install=$HOME/.vim/bundle/youcompleteme/install.py
    # IF NOT C++ needed install with  python $youcompleteme_install
    python $youcompleteme_install --clang-completer
    cp $install_folder/vim/global_extra_conf.py $HOME/
}
install_vim
